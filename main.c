#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void* fn(void* parm) {
    char* string = parm;
    while(1)  {  
        printf("%s", string);
        usleep(1000000);  
    }
}
int main(int argc, char* argv[]) {
    pthread_t thread1, thread2;
    char msg1[80] = "\x1B[32mHola, ";
    char msg2[80] = "\x1B[35mmundo!\n\r\x1B[0m";
    pthread_create(&thread1, NULL,
                   fn, (void*)msg1);
    pthread_create(&thread2, NULL,
                   fn, (void*)msg2);
    // Acá podría ir código extra.
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    return 0;
}
